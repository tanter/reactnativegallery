import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet, Image
} from 'react-native';

import Loader from '../../../UI/Loader/Loader';

class ImageItemScreen extends Component {
  state = {
    image: null
  };

  componentDidMount() {
    const imageId = this.props.navigation.state.params.id;
    fetch(`https://api.unsplash.com/photos/${imageId}/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0`)
      .then(res => {
        res.json().then(data => {
          this.setState({
            image: data
          });
        })
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    let outputData = <Loader/>;

    if (this.state.image) {
      outputData = (
        <View style={styles.item}>
          <Image
            style={{width: '100%', aspectRatio: 1}}
            source={{uri: this.state.image.urls.small}}
          />
          <View style={{paddingTop: 18, alignItems: 'center'}}>
            <Text style={{fontWeight: 'bold', fontSize: 18, marginBottom: 5}}>
              Description:
            </Text>
            <Text style={{padding: 5}}>
              {this.state.image.description ? this.state.image.description : 'Image has no description'}
            </Text>
          </View>
        </View>
      )
    }
    return outputData;
  }
}

const styles = StyleSheet.create({
  item: {
    flex: 1,
    alignItems: 'center',
    padding: 5,
    width: '100%',
  }
});

export default ImageItemScreen;
