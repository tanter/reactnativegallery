import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity
} from 'react-native';

import Loader from '../../UI/Loader/Loader';
import ImageItemScreen from './ImageItemScreen/ImageItemScreen';

class ImagesListScreen extends Component {
  state = {
    imagesList: null
  };

  componentDidMount() {
    fetch('https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0')
      .then(res => {
        res.json().then(data => {
          this.setState({
            imagesList: data
          });
        })
      })
      .catch(err => {
        console.log(err);
      });
  }

  imageItem = item => {
    return (
      <TouchableOpacity
        key={item.id}
        activeOpacity={0.9}
        style={{marginBottom: 30}}
        onPress={() => this.getImage(item.id)}
      >
        <Image
          style={{
            width: '100%',
            aspectRatio: 1
          }}
          source={{uri: item.urls.small}}
        />
        <View style={{paddingTop: 8, alignItems: 'center'}}>
          <Text style={{fontWeight: 'bold', fontSize: 18}}>
            User: {item.user.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  getImage = id => {
    this.props.navigation.navigate('ImageItemScreen', {id})
  };

  render() {
    let outputData = <Loader/>;

    if (this.state.imagesList) {
      outputData = (
        <FlatList
          data={this.state.imagesList}
          renderItem={({item}) => this.imageItem(item)}
          contentContainerStyle={{padding: 5}}
        />
      )
    }
    return outputData;
  }
}

export default ImagesListScreen;
