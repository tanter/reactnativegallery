import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import ImageItemScreen from '../screens/ImagesListScreen/ImageItemScreen/ImageItemScreen';
import ImagesListScreen from '../screens/ImagesListScreen/ImagesListScreen';

const AppNavigator = createStackNavigator({
  ImagesListScreen: {
    screen: ImagesListScreen,
    path: '/',
    navigationOptions: {
      title: 'Gallery',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        color: '#fff'
      },
      headerStyle: {
        backgroundColor: '#150034',
      }
    }
  },
  ImageItemScreen: {
    screen: ImageItemScreen,
    route: '/:id',
    navigationOptions: {
      title: 'Image',
      headerTintColor: '#fff',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        color: '#fff'
      },
      headerStyle: {
        backgroundColor: '#21003a',
      }
    }
  }
});

export default createAppContainer(AppNavigator);
