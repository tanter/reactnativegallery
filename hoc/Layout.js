import React from 'react';
import {
  View,
  StyleSheet
} from 'react-native';

import AppNavigator from '../navigation/AppNavigator';

export default () => {
  return (
    <View style={styles.container}>
      <AppNavigator/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
});
